#variable "shared_credentials_file" {
#shared_credentials_file = "/home/osboxes/.aws/credentials"
#profile = "terraform"
#}

variable "region" {
  description = "The AWS region to create resources in."
  default = "eu-west-1"
}

variable "s3_bucket" {
  description = "S3 bucket to store terraform remote state and artifactory data."
  default = "knab-artifactory"
}

variable "key_name" {
  description = "Name of key pair. Must exist in chosen region."
  default = "knab-artifactory"
}

variable "instance_type" {
  default = "t2.small"
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the Ubuntu 18.04 LTS."
  default = {
    eu-west-1 = "ami-00035f41c82244dab"
  }
}