pipeline {
  agent { label "${env.NODE_NAME}" }

  environment {
     EXECUTOR = 'jenkins'
     ACTION_ARG = '-auto-approve'
     BITBUCKET_CREDENTIALS = 'bitbucket-jenkins'
  }

  parameters {
        string(name: 'TERRAFORM_ACTION', defaultValue: 'apply', description: 'default input value: apply, if you want to destroy your stack; provide value: destroy ')
}

  stages {
    stage('Checkout Source and use branch') {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: "${GIT_BRANCH}"]],
          doGenerateSubmoduleConfigurations: false,
          extensions: [],
          submoduleCfg: [],
          userRemoteConfigs: [[credentialsId: "${BITBUCKET_CREDENTIALS}",
          url: "${GIT_URL}"]]
        ])
      }
    }


    stage("Deploy the stack!") {
      steps {
              echo "terraformaction: ${env.TERRAFORM_ACTION}"
              slackSend baseUrl: "https://knabnl.slack.com/services/hooks/jenkins-ci/", channel: "#jenkins-notifications", color: "#00F", message: "Build Started: ${env.JOB_NAME} ${env.BUILD_NUMBER}  Build Environment: ${env.ENV_NAME} Build action: ${params.TERRAFORM_ACTION}", teamDomain: "knabnl", tokenCredentialId: "knabnl-token"
              sh "terraform init"
              sh "terraform apply -input=false -auto-approve"
          }
      }
  }

    post {
      success {
          slackSend baseUrl: "https://knabnl.slack.com/services/hooks/jenkins-ci/", channel: "#jenkins-notifications", color: "good", message: "Build Successful: ${env.JOB_NAME} ${env.BUILD_NUMBER}", teamDomain: "knabnl", tokenCredentialId: "knabnl-token"
      }

      failure {
          slackSend baseUrl: "https://knabnl.slack.com/services/hooks/jenkins-ci/", channel: "#jenkins-notifications", color: "danger", message: "Build Failed: ${env.JOB_NAME} ${env.BUILD_NUMBER}", teamDomain: "knabnl", tokenCredentialId: "knabnl-token"
      }
    }
}
